#------------------------------------------------------------------------------
# Copyright (c) 2015-2017, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: toolsBar
# Configurable tools bar
#-------------------------------------------------------------------------------
MUTE = @
BUILD_REL	= 1
BUILD_LEV	= 3
BUILD_MIN	= 0
#-------------------------------------------------------------------------------
help:
	@echo
	@echo ------------------
	@echo Available targets:
	@echo ------------------
	@echo
	@echo $(MAKE) all ............ Build project outputs for Linux
	@echo $(MAKE) wall ........... Build project outputs for Windows
	@echo $(MAKE) clean .......... Clean project outputs for Linux
	@echo $(MAKE) wclean ......... Clean project outputs for Windows
	@echo $(MAKE) install ........ Install project outputs for Linux
	@echo $(MAKE) winstall ....... Install project outputs for Windows
	@echo $(MAKE) cleaninstall ... Remove installed project outputs for Linux
	@echo $(MAKE) wcleaninstall .. Remove installed project outputs for Windows
	@echo $(MAKE) astyle ......... Format C sources
	@echo
all clean:
	$(MUTE)cd toolsBar_c; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN); cd ..
	$(MUTE)cd toolsBar_java; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN); cd ..
install cleaninstall: guard-INSTALLDIR
	$(MUTE)cd toolsBar_c; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN); cd ..
	$(MUTE)cd toolsBar_bin; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN); cd ..

wall wclean:
	$(MUTE)cd toolsBar_c; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN) PLATFORM=WINDOWS; cd ..
	$(MUTE)cd toolsBar_java; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN) PLATFORM=WINDOWS; cd ..
winstall wcleaninstall: guard-INSTALLDIR
	$(MUTE)cd toolsBar_c; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN) PLATFORM=WINDOWS; cd ..
	$(MUTE)cd toolsBar_bin; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN) PLATFORM=WINDOWS; cd ..

astyle:
	$(MUTE)cd toolsBar_c; $(MAKE) --no-print-directory $@ VERSION=$(BUILD_REL).$(BUILD_LEV).$(BUILD_MIN); cd ..
#-------------------------------------------------------------------------------
guard-%:
	$(MUTE)if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; exit 1; \
	fi
.PHONY: all clean install cleaninstall wall wclean winstall wcleaninstall help
#-------------------------------------------------------------------------------
