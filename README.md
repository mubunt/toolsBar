
# APPLICATION: *toolsBar*

 **toolsBar** is an application that manages a configurable tool bar. As usual, this toolbar can be located at various locations on the screen and can be displayed horizontally or vertically. Where it changes is that it uses a script (".sh" or ".bat" depending on the host system) to activate the underlying tool. This activation may therefore require more than one command. Visually, this toolbar consists of:
-  an icon categorizing the toolbar (e.g. the icon of a microprocessor for a set of application development tools targeting this particular microprocessor (that is the example we chose later in this document), or a symbol identifying a task such as document management for a set of dedicated tools, etc.),
- a set of buttons with an icon identifying a tool
- and optionally a button "*information*" that can display information on this set of tools (e.g. the version of these tools, their location on disk, or any other relevant information that may assist the user).

**Example 1:**

![Example 1 of tool bar](README_images/toolsBar2.png  "Example 1 of tool bar")

	Note: The ARM logo is a registered trademark of ARM Limited (or its subsidiaries) in the EU and/or elsewhere. All rights reserved.

**Example 2:**

![Example 2 of tool bar](README_images/toolsBar.png  "Example 2 of tool bar")

In the toolbar, a tool is identified by the icon associated with it and a tool tip.

![Tool tip](README_images/toolsBar_tooltip.png  "Tool tip")

By right-clicking on the icon categorizing the bar, we have access to a menu that allows us to change the preferences, to view some information on **toolsBar** , or to leave it.

![Menu of tool bar](README_images/toolsBar_menu.png  "Menu of tool bar")

*Preferences* are all parameters that allow the toolbar to do what it was set and at the  place that was assigned.

![Preferences of tool bar](README_images/toolsBar_preferences.png  "Preferences of tool bar") 

	Note: *Tools Location* is the path of the root directory of tools available via the bar.
 
## LICENSE
**toolsBar** is covered by the GNU General Public License (GPL) version 2 and above.

## CONFIGURATION OF TOOLSBAR
To configure the application, you must provide the 3 following source files:
1. A script to launch the customized version of  **toolsBar**. Files *sxbar* (Linux) and *sxbar.bat* (Windows) issued in the tree serve as an example. They must be duplicated and then renamed (e.g. *mytoolber*)
2. A script to launch selected tools. iles *sxbarcmds.sh* (Linux) and *sxbarcmds.bat* (Windows) issued in the tree serve as an example to create* mytoolbarcmds.sh* and/or *mytoolbercmds.bat*.
This script defined, for each tool to be called, the set of commands needed to this call. It will be called with 2 parameters:
	- The switch number associated in the property file (\*_FEATURE variables hereafter)
	- The path of the root directory of tools (TOOLSPATH variable hereafter).
3. A property file (can be based on *sxbar.properties*) named *mytoolbar.properties* that defines the characteristics of the toolbar and the initial value of preferences.

**Example:**
 
	// Path of the root directory of tools available via the bar
	TOOLSPATH=
	// Path to the image for the icon categorizing the toolbar when it is displayed horizontally
	PROCESSORH=/images/Processor.png
	// Path to the image for the icon categorizing the toolbar when it isdisplay edvertically
	PROCESSORV=/images/Processor_rotated.png
	// Horizontal = 0, Vertical = 1
	DIRECTION=0
	// Top-Left = 0, Top-Middle = 1, Top-Right = 2
	// Center-Left = 3, Center-Right = 4
	// Bottom-Left = 5, Bottom-Middle = 6, Bottom-Right = 7
	LOCATION=0
	// Path to the image for the "information" button.
	// May be absent; in these case, no "information" button is displayed
	ABOUT_IMAGE=/images/About.png
	// Tooltip for the "information" button
	ABOUT_TIP=About these Tools
	// Switch number for *mytoolbercmds* script
	ABOUT_FEATURE=0
	// List of id of tools in the customized toolbar. "id" can be any string of characters. (except ", ', etc.)
	// For each id, there must be a triplet "_IMAGE", "_TIP" and "_FEATURE".
	// Buttons are displayed in the same order as features
	FEATURES=A, B, C
	// Tool A: image, tooltip and index
	A_IMAGE=/images/Eclipse.png
	A_TIP=Workbench
	A_FEATURE=1
	// Tool B: image, tooltip and index
	B_IMAGE=/images/Documentation.png
	B_TIP=On-line Documentation
	B_FEATURE=2
	// Tool C: image, tooltip and index
	C_IMAGE=/images/Bash.png
	C_TIP=Shell terminal initialized for Tools
	C_FEATURE=3

	Note: Suggested size for images is 32x32.

## USAGE
	$ toolsBar --help
	toolsBar - Copyright (c) 2015-2017, Michel RIZZO. All Rights Reserved.
	toolsBar - Version 1.3.0

	Configurable tools bar.

	Usage: toolsBar [OPTIONS]...

	  -h, --help         Print help and exit
	  -V, --version      Print version and exit
	  -j, --jars         Print version of underlying Java files and exit
                       (default=off)
 	 -t, --tool=STRING  Tool name
	$ 

## STRUCTURE OF THE APPLICATION
This section walks you through **toolsBar**'s structure. Once you understand this structure, you will easily find your way around in **toolsBar**'s code base.

	toolsBar/                                # 
	├── README_images/                       # Images for documentation
	│   └── ...                              # Java sources
	├── toolsBar_java/                       # 
	│   ├── toolsBar/                        # IntelliJ project structure
	│   │   └── ...                          # 
	│   └── Makefile                         # Makefile for build and install
	├── toolsBar_bin/                        # Scripts, java jar and images files to be installed
	│   ├── images/                          # Images used for Examples
	│   │   └── ...                          # 
	│   ├── lin/                             # 64-bit Linux
	│   │   ├── ds5barcmds.sh                # Shell script to launch tools used for Example #1 <--
	│   │   ├── sxbarcmds.sh                 # Shell script to launch tools used for Example #2 <--
	│   │   ├── swt-463.jar                  # Platform-dependant jar file
	│   │   └── tcpbarcmds.sh                # 
	│   ├── win/                             # 64-bit Windows
	│   │   ├── sxbarcmds.bat                # Shell script to launch tools used for Example #2 <--
	│   │   ├── swt-463.jar                  # Platform-dependant jar file
	│   │   └── tcpbarcmds.bat               # 
	│   ├── Makefile                         # 
	│   ├── commons-cli-1.4.jar              # Platform-independant jar file
	│   ├── ds5bar.properties                # Preferences file used for  Example #1 <--
	│   ├── sxbar.properties                 # Preferences file used for  Example #2 <--
	│   ├── tcpbar.properties                # 
	│   └── toolsBar.jar                     # Platform-independant jar file
	├── toolsBar_c/                          # C sources
	│   ├── Makefile                         # akefile for build and install
	│   ├── toolsBar.ggo                     # 
	│   └── toolsBar.c                       # 
	├── COPYING                              # GNU General Public License
	├── LICENSE                              # License file
	├── Makefile                             # Top-level makefile
	├── RELEASENOTES                         # Release notes
	├── README.md                            # This file
	└── index.html                           # HTML short description for GitHub
	
	12 directories, 51 files
	
## HOW TO BUILD THIS APPLICATION
### Linux Platform
	$ cd toolsBar
	$ make clean all
	
### Windows Platform
For the Windows platform, the application is built on Linux (cross-platform generation).

	$ cd toolsBar
	$ make wclean wall

## HOW TO INSTALL AND USE THIS APPLICATION
### Linux Platform
	$ cd toolsBar
	$ make install INSTALLDIR=<Path-to-installation-directory>
	$ toolsBar -t <name>
				# Assuming that the installation directory is defined
				# in your PATH env variable.
				# Another way is to double-click on the file via a file explorer.
	
### Windows Platform
if you "see" the Windows target directory from your Linux system:

	$ cd toolsBar
	$ make winstall INSTALLDIR=<Path-to-Windows-installation-directory>

Else, install **infoVault** a mano after creating the install tree structure in a temporary area:

	$ cd toolsBar
	$ make winstall INSTALLDIR=~/tmp

		To be installed: 
			- toolsBar.exe file
			- toolsBar_bin directory


## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on a Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *toolsBar/toolsBar_java/toolsBar* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
	- JAVA 1.8.0 for usage and development
- For development:
	- IntelliJ IDEA 2017.1.1 Build #IC-171.4073.35, built on April 6, 2017
	- JRE: 1.8.0_112-release-736-b16 amd64
	- JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
	- MinGW-64 packages for Windows cross-generation

Application developed and tested with UBUNTU 16.10 / UBUNTU 17.04 / XUBUNTU 17.04
Application tested with WINDOWS 10.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES) .

### IMPORTANT:
With Ubuntu 16.10 / [X]Ubuntu 17.04, there is one issue with the version of GTK+ 3, shipped. The tool bar is display incorrectly. In this case, the line below in your */etc/environment* file:

	export SWT_GTK3=0

The bug is filed with Ubuntu at bug 1552764.
***
