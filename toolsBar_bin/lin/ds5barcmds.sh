#!/bin/bash
#------------------------------------------------------------------------------
# Copyright (c) 2014-2016, Michel RIZZO (the "author" in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# -----------------------------------------------------------------------------
# Tool Bar on Ubuntu / STMicroelectronics - STxP70
#
# sxbarcmds.sh 0|1|2|3 Tools_path
#------------------------------------------------------------------------------
if [ "$#" -ne 2 ]; then
    echo "ERROR: Illegal number of parameters"
    exit -1
fi
case ${1} in
    *[0-9]*)
		;;
    *)	echo "ERROR: First parameter is not a number"
		exit -1
		;;
esac
#------------------------------------------------------------------------------
DS5=${2}

case ${1} in
	0)	echo "v5.25.0";
		echo "${DS5}";
		;;
	1)	${DS5}/bin/eclipse
		;;
	2)	${DS5}/bin/mgd
		;;
	3)	${DS5}/bin/streamline
		;;
	4)	${DS5}/bin/rviconfigip
		;;
	5)	${DS5}/bin/dbghwconfig
		;;
	*)	echo "ERROR: '${1}' is a wrong function number"
		;;
esac
exit $?
#------------------------------------------------------------------------------
