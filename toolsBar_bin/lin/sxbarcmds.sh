#!/bin/bash
#------------------------------------------------------------------------------
# Copyright (c) 2014-2016, Michel RIZZO (the "author" in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# -----------------------------------------------------------------------------
# Tool Bar on Ubuntu / STMicroelectronics - STxP70
#
# sxbarcmds.sh 0|1|2|3 Tools_path
#------------------------------------------------------------------------------
if [ "$#" -ne 2 ]; then
    echo "ERROR: Illegal number of parameters"
    exit -1
fi
case ${1} in
    *[0-9]*)
		;;
    *)	echo "ERROR: First parameter is not a number"
		exit -1
		;;
esac
#------------------------------------------------------------------------------
CMD_WORKBENCH=sxw
CMD_USERDOC=sxdoc
CMD_TERMINAL=terminator
CMD_TERMINAL_OTHER=xterm

STxP70RCFILE=bin/STxP70.sh
#------------------------------------------------------------------------------
SX=${2}
RCFILE=${SX}/${STxP70RCFILE}

if [ ! -f ${RCFILE} ]; then
	echo "ERROR: '${RCFILE}' file does not exist!!!"
else
	case ${1} in
		0)	${SHELL} -c "source ${RCFILE} --nobanner 1>/dev/null; \
			echo \"Toolset version: \${SXVER}\"; \
			echo \"Toolset path: \${SX}\"; \
			echo \"Workbench location: \${STWORKBENCH}\"; \
			echo \"Java runtime location: \${STJRE}\"; \
			echo \"TLM location: \${SXTLM}\""
			;;
		1)	${SHELL} -c "source ${RCFILE} --nobanner; sxw"
			;;
		2)	${SHELL} -c "source ${RCFILE} --nobanner; sxdoc"
			;;
		3)	HOSTOS=`$SX/bin/sxguess-os.sh`
			case "$HOSTOS" in
				linux-ubuntu-16.04-x86_64 | linux-ubuntu-16.10-x86_64)
					TERMINAL=${CMD_TERMINAL}
					${SHELL} -c "source ${RCFILE} --nobanner; cd ${HOME}; ${TERMINAL}"
					;;
				linux-ubuntu-14*)
					TERMINAL=${CMD_TERMINAL}
					${SHELL} -c "source ${RCFILE} --nobanner; cd ${HOME}; \
						LD_PRELOAD=/usr/lib/i386-linux-gnu/libstdc++.so.6 ${TERMINAL}"
					;;
				linux-ubuntu-12*)
					TERMINAL=${CMD_TERMINAL}
					${SHELL} -c "source ${RCFILE} --nobanner; cd ${HOME}; ${TERMINAL}"
					;;
				default)
					TERMINAL=${CMD_TERMINAL_OTHER}
					${SHELL} -c "source ${RCFILE} --nobanner; cd ${HOME}; ${TERMINAL}"
					;;
			esac
			;;
		*)	echo "ERROR: '${1}' is a wrong function number"
			;;
	esac
fi
exit $?
#------------------------------------------------------------------------------
