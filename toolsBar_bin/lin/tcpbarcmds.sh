#!/bin/bash
#------------------------------------------------------------------------------
# Copyright (c) 2017, Michel RIZZO (the "author" in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# -----------------------------------------------------------------------------
# Tool Bar on Ubuntu / TCP Application
#
# tcpbarcmds.sh 0|1|2|3|4 Tools_path
#------------------------------------------------------------------------------
if [ "$#" -ne 2 ]; then
    echo "ERROR: Illegal number of parameters"
    exit -1
fi
case ${1} in
    *[0-9]*)
		;;
    *)	echo "ERROR: First parameter is not a number"
		exit -1
		;;
esac
#------------------------------------------------------------------------------
verbose=-v
TERMINAL=terminator
#------------------------------------------------------------------------------
TCPPATH=${2}

case ${1} in
	0)	echo "Version 1.0.0 - January, 2017"
		echo "${TCPPATH}";
		;;
	1)	${SHELL} -c "${TERMINAL} --title "Server" --geometry=800x200+0+0 --execute ${TCPPATH}/tcpServer --tcpServer $verbose --spy 2> /dev/null &"
		;;
	2)	${SHELL} -c "${TERMINAL} --title "Acquisition" --geometry=800x200+0+260 --execute ${TCPPATH}/tcpServer --acquisition $verbose 2> /dev/null &"
		;;
	3)	${SHELL} -c "${TERMINAL} --title "Reporting" --geometry=800x200+0+500 --execute ${TCPPATH}/tcpServer --report 2> /dev/null &"
		;;
	4)	${SHELL} -c "${TCPPATH}/tcpServer --sensor"
		;;
	*)	echo "ERROR: '${1}' is a wrong function number"
		;;
esac
exit $?
#------------------------------------------------------------------------------
