@echo off
rem ------------------------------------------------------------------------------
rem  Copyright (c) 2015-2016, Michel RIZZO.
rem This program is free software; you can redistribute it and/or
rem modify it under the terms of the GNU General Public License
rem as published by the Free Software Foundation; either version 2
rem of the License, or (at your option) any later version.
rem 
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem 
rem You should have received a copy of the GNU General Public License
rem along with this program; if not, write to the Free Software
rem Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
rem ------------------------------------------------------------------------------
rem Tool Bar on Windows / STMicroelectronics - STxP70
rem ------------------------------------------------------------------------------
set ARGCOUNT=0
for %%x in (%*) do Set /A ARGCOUNT+=1
if %ARGCOUNT% neq 2 call :argerror
set /a FEATURE=%1
if %FEATURE% neq %1 call :numbererror
rem ------------------------------------------------------------------------------
set SX=%2

set CMD_WORKBENCH=STxP70_workbench.bat
set CMD_USERDOC=STxP70_docportal.bat
set CMD_TERMINAL=STxP70.bat

call :case_%FEATURE%
if ERRORLEVEL 1 call :caserror		# if label doesn't exist
goto :eof
:case_0
	echo.Tools path=%SX%
	exit /B
:case_1
	cmd /c %SX%\bin\%CMD_WORKBENCH%
	exit /B
:case_2
	cmd /c %SX%\bin\%CMD_USERDOC%
	exit /B
:case_3
	cmd /c %SX%\bin\%CMD_TERMINAL%
	exit /B
rem ----- Routines ---------------------------------------------------------------
:argerror
	echo.ERROR: Bad number of parameters. Abort ...
	goto :pause
:numbererror
	echo.ERROR: First parameteri is not a number. Abort ...
	goto :pause
:caserror
	echo.ERROR: Bad first parameter. Abort ...
:pause
	pause
	exit -1
rem ------------------------------------------------------------------------------
:eof
rem ------------------------------------------------------------------------------