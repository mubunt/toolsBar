@echo off
rem ------------------------------------------------------------------------------
rem  Copyright (c) 2017, Michel RIZZO.
rem This program is free software; you can redistribute it and/or
rem modify it under the terms of the GNU General Public License
rem as published by the Free Software Foundation; either version 2
rem of the License, or (at your option) any later version.
rem 
rem This program is distributed in the hope that it will be useful,
rem but WITHOUT ANY WARRANTY; without even the implied warranty of
rem MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
rem GNU General Public License for more details.
rem 
rem You should have received a copy of the GNU General Public License
rem along with this program; if not, write to the Free Software
rem Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
rem ------------------------------------------------------------------------------
rem Tool Bar on Windows for TCP applications
rem ------------------------------------------------------------------------------
set ARGCOUNT=0
for %%x in (%*) do Set /A ARGCOUNT+=1
if %ARGCOUNT% neq 2 call :argerror
set /a FEATURE=%1
if %FEATURE% neq %1 call :numbererror
rem ------------------------------------------------------------------------------
set TCPPATH=%2
set verbose=--verbose
set dir=C:\tmp

call :case_%FEATURE%
if ERRORLEVEL 1 call :caserror		# if label doesn't exist
goto :eof
:case_0
	echo.Version 1.10 - May, 2017
	echo.Tools Path=%TCPPATH%
	goto :eof
:case_1
	start "Server" /D %dir% %TCPPATH%\tcpServer --tcpServer %verbose% --spy
	goto :eof
:case_2
	start "Acquisition Client" /D %dir% %TCPPATH%\tcpServer --acquisition %verbose%
	goto :eof
:case_3
	start "Report Client" /D %dir% %TCPPATH%\tcpServer --report %verbose%
	goto :eof
:case_4
	start "Sensors" /D %dir% %TCPPATH%\tcpServer --sensor %verbose%
	goto :eof
rem ----- Routines ---------------------------------------------------------------
:argerror
	echo.ERROR: Bad number of parameters. Abort ...
	goto :pause
:numbererror
	echo.ERROR: First parameteri is not a number. Abort ...
	goto :pause
:caserror
	echo.ERROR: Bad first parameter. Abort ...
:pause
	pause
	exit -1
rem ------------------------------------------------------------------------------
:eof
rem ------------------------------------------------------------------------------
