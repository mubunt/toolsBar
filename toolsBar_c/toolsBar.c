//------------------------------------------------------------------------------
// Copyright (c) 2017, Michel RIZZO (the 'author' in the following)
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: toolsBar
// Management of sensitive information at home
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifdef LINUX
#include <sys/wait.h>
#else
#include <windows.h>
#include <winbase.h>
#include <errno.h>
#endif
//------------------------------------------------------------------------------
// MACROS DEFINITIONS TO CUSTOMIZE THE PROGRAM
//------------------------------------------------------------------------------
#define APPLICATION 			"toolsBar"
#define BINDIR					"/toolsBar_bin/"
#define JAR 					"toolsBar.jar"
#define HEADER					"toolsBar_cmdline.h"
#define PRINTVERSION 			cmdline_parser_toolsBar_print_version
#define PARSER 					cmdline_parser_toolsBar
#define FREE 					cmdline_parser_toolsBar_free
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include HEADER
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define DISPLAY 				"DISPLAY"
#define JAVA 					"java"
#ifdef LINUX
#define ProcessPseudoFileSystem	"/proc/self/exe"
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define error(fmt, ...) 		do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define EXIST(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern void PRINTVERSION();
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static const char *jarFiles[] = {
	JAR,
	"commons-cli-1.4.jar",
	"swt-463.jar",
	""
};
//------------------------------------------------------------------------------
// INTERNAL ROUTINES
//------------------------------------------------------------------------------
static void getJARversion(char *jarpath, char *jar, char *build) {
	char command[4096];
	FILE *fdcmd = NULL;

	sprintf(command, "%s -cp %s -jar %s%s --version", JAVA, jarpath, jarpath, jar);
	fdcmd = popen(command, "r");
	if (fgets(build, BUFSIZ, fdcmd)) { ; }
	pclose(fdcmd);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Return code (EXIT_SUCCESS - Normal program stop / EXIT_FAILURE - Program stop on error)
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	struct stat locstat, *ptlocstat;        // stat variables for EXIST*
	char jarpath[PATH_MAX];
	char jarfile[PATH_MAX];
	char command[4096];
	char build[BUFSIZ];
	char *pt;
	unsigned int i;
	int j, retval = 0;
	size_t k;

	//---- Parameter checking and setting --------------------------------------
	if (PARSER(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
#ifdef LINUX
	//----- Get real path of this executable to compute the path JARS files
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	if (readlink(ProcessPseudoFileSystem, jarpath, sizeof(jarpath)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		goto ERR;
	}
#else
	if (0 == GetModuleFileName(NULL, jarpath, sizeof(jarpath))) {
		error("%s", "Cannot get path of the current executable.");
		goto ERR;
	}
#endif
	dirname(jarpath);
	strncat(jarpath, BINDIR, sizeof(jarpath));
	if (! EXISTDIR(jarpath)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", jarpath);
		goto ERR;
	}
	//---- Check consistency of the installation
	i = 0;
	while (strlen(jarFiles[i]) != 0) {
		snprintf(jarfile, sizeof(jarfile), "%s%s",jarpath, jarFiles[i]);
		if (! EXIST(jarfile)) {
			error("WRONG INSTALLATION - Jar file '%s' does not exist.", jarfile);
			goto ERR;
		}
		++i;
	}
	//---- Option "--jars" (-j) -------------------------------------------------
	if (args_info.jars_given) {
		PRINTVERSION();
		getJARversion(jarpath, (char *)JAR, build);
		fprintf(stdout, "%s - %s", APPLICATION, build);
		goto END;
	}
	//---- Run Java
	sprintf(command, "%s -cp %s -jar %s%s", JAVA, jarpath, jarpath, JAR);
	for (j = 1; j < argc; j++) {
		strcat(command, " ");
		pt = argv[j];
		k = strlen(command);
		while (*pt != '\0') {
			if (*pt == ' ') command[k++] = '\\';
			command[k] = *pt;
			command[++k] = '\0';
			++pt;
		}
	}
	retval = system(command);
	//---- End -------------------------------------------------------------------
END:
	FREE(&args_info);
	return(retval);
ERR:
	FREE(&args_info);
	return EXIT_FAILURE;
}
//------------------------------------------------------------------------------
