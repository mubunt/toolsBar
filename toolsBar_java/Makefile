#------------------------------------------------------------------------------
# Copyright (c) 2016-2017, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: toolsBar
# Configurable tools bar
#-------------------------------------------------------------------------------
MUTE		= @
APPLICATION	= toolsBar
#-------------------------------------------------------------------------------
IDEA_PRJ	= $(APPLICATION)
IDEA_SRC	= $(IDEA_PRJ)/src
IDEA_BIN	= $(IDEA_PRJ)/out/production/$(IDEA_PRJ)
#-------------------------------------------------------------------------------
JARDIR		= toolsBar_bin
ICONDIR		= $(JARDIR)/icons
#-------------------------------------------------------------------------------
.SUFFIXES: .java .class

SOURCES		= $(IDEA_SRC)/toolsBar.java \
			  $(IDEA_SRC)/toolsBarAbout.java \
			  $(IDEA_SRC)/toolsBarAboutMe.java \
			  $(IDEA_SRC)/toolsBarBuildInfo.java \
			  $(IDEA_SRC)/toolsBarPreferences.java \
			  $(EOL)
CLASSES 	= $(addprefix $(IDEA_BIN)/,$(notdir $(SOURCES:.java=.class)))
JARFILE		= ../$(JARDIR)/$(APPLICATION).jar
MANIFESTTMP	= manifest.txt
#-------------------------------------------------------------------------------
RM			= rm -f
JC 			= javac
JCFLAGS		= -d $(IDEA_BIN) \
			  -cp "../$(APPLICATION)_bin/lin/*:../$(APPLICATION)_bin/*" \
			  $(EOL)
JAR 		= jar
JARFLAGS	= cmf
#-------------------------------------------------------------------------------
help:
	@echo
	@echo ------------------
	@echo Available targets:
	@echo ------------------
	@echo
	@echo $(MAKE) all ............ Build project outputs for Linux
	@echo $(MAKE) wall ........... Build project outputs for Windows
	@echo $(MAKE) clean .......... Clean project outputs for Linux
	@echo $(MAKE) wclean ......... Clean project outputs for Windows
	@echo
#-------------------------------------------------------------------------------
$(CLASSES):
	@echo "	Compiling java sources..."
	$(MUTE)$(JC) $(JCFLAGS) $(SOURCES)
$(JARFILE): $(CLASSES)
	$(MUTE)cd $(IDEA_BIN); \
		echo "	Creating MANIFEST source file..."; \
		echo "Manifest-Version: 1.0" > $(MANIFESTTMP); \
		echo "Class-Path: swt-463.jar commons-cli-1.4.jar" >> $(MANIFESTTMP); \
		echo "Main-Class: $(APPLICATION)" >> $(MANIFESTTMP); \
		echo "" >> $(MANIFESTTMP); \
		echo "	Creating JAR file $(JARFILE)..."; \
		$(JAR) $(JARFLAGS) $(MANIFESTTMP) ../../../../$(JARFILE) *.class; \
		$(RM) $(MANIFESTTMP)
#-------------------------------------------------------------------------------
wall all: $(JARFILE)
wclean clean:
	@echo "	Removing CLASS files..."
	$(MUTE)$(RM) $(IDEA_BIN)/*.class $(IDEA_BIN)/META-INF/MANIFEST.MF
#-------------------------------------------------------------------------------
.PHONY: all wall clean wclean help
#-------------------------------------------------------------------------------