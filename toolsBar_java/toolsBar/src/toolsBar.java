//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
//------------------------------------------------------------------------------
public class toolsBar {
	//--------------------------------------------------------------------------
	static class toolsDefinition {
		String toolsImage;
		String toolsTip;
		int toolsFeature = -1;
		Image image;
	}
	//--------------------------------------------------------------------------
	static final String eol = System.getProperty("line.separator");
	private final static String slash = System.getProperty("file.separator");
	private final static String os = System.getProperty("os.name");
	static final String home = System.getProperty("user.dir");
	//--------------------------------------------------------------------------
	private static final String sTitle = "Configurable tools bar";
	private static final String jarFile = "toolsBar.jar";
	private static final String propertiesFileSuffix	= ".properties";
	static final String propertyTools = "TOOLSPATH";
	static final String propertyDirection = "DIRECTION";
	static final String propertyLocation = "LOCATION";
	private static final String propertyProcHorizontal = "PROCESSORH";
	private static final String propertyProcVertical = "PROCESSORV";
	private static final String propertyAboutImage = "ABOUT_IMAGE";
	private static final String propertyAboutTip = "ABOUT_TIP";
	private static final String propertyAboutFeature = "ABOUT_FEATURE";
	private static final String propertyFeatures = "FEATURES";
	private static final String intelliJobjpath = "toolsBar_java/toolsBar/out/production/toolsBar/";
	//--------------------------------------------------------------------------
	static final Color ColorGainsboro = new Color(Display.getDefault(), 220, 220, 200);
	static String ToolsetRootDir;
	static int Direction = -1;
	static int Location = -1;
	private static String ImgProcHorizontal;
	private static String ImgProcVertical;	
	private static String ImgAbout;
	private static String TipAbout;
	static int FeatureAbout = -1;
	private static toolsDefinition[] tools;
	//--------------------------------------------------------------------------
	private static String InstallRootDir;
	static Properties properties;
	static String toolName = "toolsBar";
	static String propertiesFile;
	static String scriptFile;
	private static Image imageProcessorH;
	private static Image imageProcessorV;
	private static Image imageAbout;
	private static Shell shell;
	static int screenWidth;
	static int screenHeight;
	private static int toolsNumber;
	//--------------------------------------------------------------------------
	static final int cst_horizontal = 0;
	static final int cst_vertical = 1;
	static final int cst_topleft = 0;
	static final int cst_topcenter = 1;
	static final int cst_topright = 2;
	static final int cst_middleleft = 3;
	static final int cst_middleright = 4;
	static final int cst_bottomleft = 5;
	static final int cst_bottomcenter = 6;
	static final int cst_bottomright = 7;

	private static final String suffixLin = ".sh";
	private static final String suffixWin = ".bat";
	private static final String suffixScript = "cmds";
	private static final String suffixImage = "_IMAGE";
	private static final String suffixTip = "_TIP";
	private static final String suffixFeature = "_FEATURE";
	//==========================================================================
	public static void main(String [] args) {
		// OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",	  false, "print this message and exit");
		options.addOption("V", "version",false, "print the version information and exit");
		options.addOption("t", "tool",	  true,  "tool name");
		try {
			CommandLine line = parser.parse(options, args);
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sTitle, options);
				Exit(0);
			}
			if (line.hasOption("version")) {
				System.out.println(jarFile + "	Build: " + toolsBarBuildInfo.getNumber() + " - Date: " + toolsBarBuildInfo.getDate());
				Exit(0);
			}
			if (line.hasOption("tool")) toolName = line.getOptionValue("tool");
		} catch (ParseException exp) {
			fatalError(shell, "Option parsing failed - " + exp.getMessage() + ". Quitting.");
		}
		//----------------------------------------------------------------------
		// IMAGES PATH,  SCRIPT and PROPERTIES FILES
		InstallRootDir = toolsBar.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		if (os.startsWith("Windows") && InstallRootDir.startsWith("/"))
			InstallRootDir = InstallRootDir.substring(1);
		InstallRootDir = InstallRootDir.replace("toolsBar.jar", "");
		// ---- PATH OF THIS JAR
		scriptFile = InstallRootDir + toolName + suffixScript ;
		if (InstallRootDir.length() > intelliJobjpath.length()) {
			String s = InstallRootDir.substring(InstallRootDir.length() - intelliJobjpath.length());
			if (s.equals(intelliJobjpath)) {
				InstallRootDir = InstallRootDir.substring(0, InstallRootDir.length() - intelliJobjpath.length() - 1)
						+ slash + "toolsBar_bin" + slash;
				scriptFile = InstallRootDir + (os.startsWith("Windows") ? "win" : "lin") + slash + toolName + suffixScript;
			}
		}
		scriptFile += os.startsWith("Windows") ? suffixWin : suffixLin;
		if (Files.notExists(Paths.get(scriptFile)))
			fatalError(shell, "file '" + scriptFile + "' does not exist");
		propertiesFile = InstallRootDir + toolName + propertiesFileSuffix;
		//----------------------------------------------------------------------
		// DISPLAY
		getScreenSize(Display.getCurrent());
		//----------------------------------------------------------------------
		// PROPERTIES
		getProperties();
		//----------------------------------------------------------------------
		// IMAGES
		imageProcessorH = new Image(Display.getCurrent(), ImgProcHorizontal);
		imageProcessorV = new Image(Display.getCurrent(), ImgProcVertical);
		if (ImgAbout.length() != 0) imageAbout = new Image(Display.getCurrent(), ImgAbout);
		for (int ¢ = 0; ¢ < toolsNumber; ++¢)
			tools[¢].image = new Image(Display.getCurrent(), tools[¢].toolsImage);
		//----------------------------------------------------------------------
		// GO ON...
		UI();
		//----------------------------------------------------------------------
		// EXIT
		Exit(0);
	}
	//==========================================================================
	private static void UI() {
		Cursor cursor = Display.getCurrent().getSystemCursor(SWT.CURSOR_ARROW);
		shell = new Shell(Display.getCurrent(), SWT.NO_TRIM);
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		shell.setCursor(cursor);
		shell.setText(toolName);

		Composite container = new Composite(shell, SWT.NONE);
		GridLayout layout = new GridLayout(Direction == cst_horizontal ? 2 : 1, false);
		layout.marginWidth = 0;
		container.setLayout(layout);
		//--- Tool logo -------------------------------------------------------------
		Label logo = new Label(container, SWT.NONE);
		logo.setImage(Direction == cst_horizontal ? imageProcessorH : imageProcessorV);
		logo.pack();
		Menu popupMenu = new Menu(logo);
		MenuItem preferenceItem = new MenuItem(popupMenu, SWT.CASCADE);
		preferenceItem.setText("Preferences");
		MenuItem aboutItem = new MenuItem(popupMenu, SWT.CASCADE);
		aboutItem.setText("About");
		MenuItem exitItem = new MenuItem(popupMenu, SWT.CASCADE);
		exitItem.setText("Exit");
		logo.setMenu(popupMenu);
		preferenceItem.addSelectionListener(new preferenceItemListener());
		aboutItem.addSelectionListener(new aboutItemListener());
		exitItem.addSelectionListener(new exitItemListener());
		//--- User-Defined Buttons --------------------------------------------------
		// Tools buttons
		ToolBar toolBar = new ToolBar(container,
				SWT.NO_FOCUS | (Direction != cst_horizontal ? SWT.VERTICAL : SWT.HORIZONTAL));
		toolBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		toolBar.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));

		for (int idx = 0; idx < toolsNumber; ++idx) {
			ToolItem itemPush = new ToolItem(toolBar, SWT.PUSH);
			itemPush .setImage(tools[idx].image);
			itemPush.setToolTipText(tools[idx].toolsTip);
			final int ifeature = idx;
			itemPush.addListener(SWT.Selection, __ -> Utility(tools[ifeature].toolsFeature));
		}
		//--- About Button ---------------------------------------------------------
		if (ImgAbout.length() != 0) {
			ToolItem itemSeparator = new ToolItem(toolBar, SWT.SEPARATOR);
			itemSeparator.setWidth(10);
			ToolItem itemPushAbout = new ToolItem(toolBar, SWT.PUSH);
			itemPushAbout.setImage(imageAbout);
			itemPushAbout.setToolTipText(TipAbout);
			itemPushAbout.addListener(SWT.Selection, arg0 -> toolsBarAbout.View());
		}
		toolBar.pack();
		//--------------------------------------------------------------------------
		container.pack();

		Point n = shell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		switch (Location) {
		case cst_topleft:
			shell.setLocation(0, 0);
			break;
		case cst_topcenter:
			shell.setLocation(((screenWidth - n.x) / 2), 0);
			break;
		case cst_topright:
			shell.setLocation((screenWidth - n.x), 0);
			break;
		case cst_middleleft:
			shell.setLocation(0, ((screenHeight - n.y) / 2));
			break;
		case cst_middleright:
			shell.setLocation((screenWidth - n.x), ((screenHeight- n.y) / 2));
			break;
		case cst_bottomleft:
			shell.setLocation(0, !os.startsWith("Windows") ? screenHeight : screenHeight - n.y);
			break;
		case cst_bottomcenter:
			shell.setLocation(((screenWidth - n.x) / 2), !os.startsWith("Windows") ? screenHeight : screenHeight - n.y);
			break;
		case cst_bottomright:
			shell.setLocation((screenWidth - n.x), !os.startsWith("Windows") ? screenHeight : screenHeight - n.y);
			break;
		}
		shell.pack();
		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
	//==========================================================================
	private static void fatalError(Shell s, String message) {
		Error(s, message);
		Exit(-1);
	}
	//==========================================================================
	private static void Error(Shell s, String message) {
		Shell out = s != null ? s : new Shell(Display.getCurrent(), SWT.NONE);
		MessageBox messageBox = new MessageBox(out, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(toolName + ": " + message);
		messageBox.open();
	}
	//==========================================================================
	static void Exit(int value) {
		if (toolsBarAbout.fontAbout != null) toolsBarAbout.fontAbout.dispose();
		if (toolsBarAbout.fontTitle != null) toolsBarAbout.fontTitle.dispose();
		if (toolsBarAboutMe.fontAbout != null) toolsBarAboutMe.fontAbout.dispose();
		if (toolsBarAboutMe.fontTool != null) toolsBarAboutMe.fontTool.dispose();
		if (toolsBarPreferences.fontLabel != null) toolsBarPreferences.fontLabel.dispose();
		if (toolsBarPreferences.fontWarning != null) toolsBarPreferences.fontWarning.dispose();
		if (toolsBarPreferences.fontButton != null) toolsBarPreferences.fontButton.dispose();
		if (toolsBarPreferences.fontGroup != null) toolsBarPreferences.fontGroup.dispose();
		if (imageProcessorH != null) imageProcessorH.dispose();
		if (imageProcessorV != null) imageProcessorV.dispose();
		if (imageAbout != null) imageAbout.dispose();
		for (int ¢ = 0; ¢ < toolsNumber; ++¢)
			if (tools[¢].image != null) tools[¢].image.dispose();
		toolsBarAboutMe.ColorFG.dispose();
		toolsBarAbout.ColorFG.dispose();
		ColorGainsboro.dispose();
		System.exit(value);
	}
	//==========================================================================
	private static void Utility(int Index) {
		Runtime runtime = Runtime.getRuntime();
		String command = scriptFile + " " + Index + " " + ToolsetRootDir;
		try { runtime.exec(command); } catch (IOException ioe) { ioe.printStackTrace(); }
	}
	//==========================================================================
	private static void getScreenSize(Display d) {
		final Monitor monitor = d.getPrimaryMonitor();
		final Rectangle rect = monitor != null ? monitor.getClientArea() : Display.getCurrent().getBounds();
		screenWidth = rect.width;
		screenHeight = rect.height;
	}
	//==========================================================================
	private static void getProperties() {
		properties = new Properties();
		File f = new File(propertiesFile);
		// Define and save the properties
		if(! f.isFile()) {
			properties.setProperty(propertyTools, "");
			properties.setProperty(propertyDirection, "");
			properties.setProperty(propertyLocation, "");
			properties.setProperty(propertyProcHorizontal, "");
			properties.setProperty(propertyProcVertical, "");
			properties.setProperty(propertyAboutImage, "");
			properties.setProperty(propertyAboutTip, "");
			properties.setProperty(propertyAboutFeature, "-1");
			savePropertyFile(properties, propertiesFile);
		}
		// Load the properties file
		loadPropertyFile(propertiesFile);
		// Convert and test properties.....

		// ... ImgProcHorizontal
		ImgProcHorizontal = properties.getProperty(propertyProcHorizontal, "");
		if (ImgProcHorizontal.length() == 0)
			fatalError(shell, "Preference '" + propertyProcHorizontal + "' is not defined");
		ImgProcHorizontal = InstallRootDir + ImgProcHorizontal;
		if (Files.notExists(Paths.get(ImgProcHorizontal)))
			fatalError(shell,
					"Preference '" + propertyProcHorizontal + "': file '" + ImgProcHorizontal + "' does not exist");

		// ... ImgProcVertical
		ImgProcVertical = properties.getProperty(propertyProcVertical, "");
		if (ImgProcVertical.length() == 0)
			fatalError(shell, "Preference '" + propertyProcVertical + "' is not defined");
		ImgProcVertical = InstallRootDir + ImgProcVertical;
		if (Files.notExists(Paths.get(ImgProcVertical)))
			fatalError(shell,
					"Preference '" + propertyProcVertical + "': file '" + ImgProcVertical + "' does not exist");

		// ... ABOUT
		ImgAbout = properties.getProperty(propertyAboutImage, "");
		if (ImgAbout.length() != 0) {
			ImgAbout = InstallRootDir + ImgAbout;
			if (Files.notExists(Paths.get(ImgAbout)))
				fatalError(shell, "Preference '" + propertyAboutImage + "': file '" + ImgAbout + "' does not exist");
			TipAbout = properties.getProperty(propertyAboutTip, "");
			String strfeature = properties.getProperty(propertyAboutFeature, "");
			if (strfeature.length() == 0)
				fatalError(shell, "Preference '" + propertyAboutFeature + "' is not defined");
			FeatureAbout = Integer.parseInt(strfeature);
		}

		// ... FEATURES
		String features = properties.getProperty(propertyFeatures, "");
		if (features.length() == 0)
			fatalError(shell, "Preference '" + propertyFeatures + "' is not defined");
		String featuresArr[] = features.split(",");
		toolsNumber = featuresArr.length;
		if (toolsNumber == 0)
			fatalError(shell, "Preference '" + propertyFeatures + "' is empty");
		tools = new toolsDefinition[toolsNumber];
		for (int i = 0; i < toolsNumber; ++i) {
			tools[i] = new toolsDefinition();
			tools[i].toolsImage = null;
			tools[i].toolsTip = null;
			tools[i].toolsFeature = -1;

			String feature = featuresArr[i].replaceAll("(^ )|( $)", "");

			String img = properties.getProperty(feature + suffixImage, "");
			if (img.length() == 0)
				fatalError(shell, "Preference '" + feature + suffixImage + "' is not defined");
			tools[i].toolsImage = InstallRootDir + img;
			if (Files.notExists(Paths.get(tools[i].toolsImage)))
				fatalError(shell, "Preference '" + feature + suffixImage + "': file '" + tools[i].toolsImage + "' does not exist");

			tools[i].toolsTip = properties.getProperty(feature + suffixTip, "");

			String strfeature = properties.getProperty(feature + suffixFeature, "");
			if (strfeature.length() == 0)
				fatalError(shell, "Preference '" + feature + suffixFeature + "' is not defined");
			tools[i].toolsFeature = Integer.parseInt(strfeature);
		}

		for (boolean reiterate = false;;) {
			ToolsetRootDir = properties.getProperty(propertyTools, "");
			if (ToolsetRootDir.length() == 0) {
				Error(shell, "Preference '" + propertyTools + "' not defined");
				reiterate = true;
			}
			if (!Files.isDirectory(Paths.get(ToolsetRootDir))) {
				Error(shell, "Directory '" + ToolsetRootDir + "' does not exist");
				reiterate = true;
			}
			try {
				Direction = Integer.parseInt(properties.getProperty(propertyDirection, ""));
			} catch (NumberFormatException nx) {
				Error(shell, "Preference '" + propertyDirection + "': not a valid number");
				reiterate = true;
			}
			if (Direction != cst_horizontal && Direction != cst_vertical) {
				Error(shell, "Preference '" + propertyDirection + "' is not a valid value");
				reiterate = true;
			}
			try {
				Location = Integer.parseInt(properties.getProperty(propertyLocation, ""));
			} catch (NumberFormatException nx) {
				Error(shell, "Preference '" + propertyLocation + "' is not a valid number");
				reiterate = true;
			}
			if (Location != cst_topleft && Location != cst_topcenter && Location != cst_topright
					&& Location != cst_bottomleft && Location != cst_bottomcenter && Location != cst_bottomright
					&& Location != cst_middleleft && Location != cst_middleright) {
				Error(shell, "Preference '" + propertyLocation + "' is not a valid value");
				reiterate = true;
			}
			if (!reiterate)
				break;
			toolsBarPreferences.View();
			loadPropertyFile(propertiesFile);
			reiterate = false;
		}
	}
	//==========================================================================
	private static void loadPropertyFile(String filename) {
		FileInputStream in;
		try {
			in = new FileInputStream(filename);
			properties.load(in);
			in.close();
		} catch (IOException err) {
			fatalError(shell, "Properties: " + err.getMessage());
		}
	}
	//==========================================================================
	static void savePropertyFile(Properties p, String filename) {
		FileOutputStream out;
		try {
			out = new FileOutputStream(filename);
			p.store(out, toolName + " toolsBarPreferences.");
			out.close();
		} catch (IOException err) { fatalError(shell, err.getMessage()); }
	}
}
//==========================================================================
class preferenceItemListener implements SelectionListener {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		toolsBarPreferences.View();
	}
	public void widgetDefaultSelected(SelectionEvent __) {
		toolsBarPreferences.View();
	}
}
//------------------------------------------------------------------------------
class aboutItemListener implements SelectionListener {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		toolsBarAboutMe.View();
	}
	public void widgetDefaultSelected(SelectionEvent __) {
		toolsBarAboutMe.View();
	}
}
//------------------------------------------------------------------------------
class exitItemListener implements SelectionListener {
	@Override
	public void widgetSelected(SelectionEvent arg0) {
		toolsBar.Exit(0);
	}
	public void widgetDefaultSelected(SelectionEvent __) {
		toolsBar.Exit(0);
	}
}
//==============================================================================
