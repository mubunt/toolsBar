//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//------------------------------------------------------------------------------
class toolsBarAbout {
	//--------------------------------------------------------------------------
	private static final String sAboutTitle = " About this Toolset ";
	static final Font fontAbout = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	static final Font fontTitle = new Font(Display.getCurrent(), "Arial", 20, SWT.BOLD);
	static final Color ColorFG = new Color(Display.getDefault(), 10, 149, 184);
	//==========================================================================
	static void View() {
		String command = toolsBar.scriptFile + " " + toolsBar.FeatureAbout + " " + toolsBar.ToolsetRootDir;

		Shell shell = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE);
		shell.setLayout(new GridLayout());
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(toolsBar.ColorGainsboro);
		shell.setText(sAboutTitle);

		Label title = new Label(shell, SWT.NONE);
		title.setFont(fontTitle);
		title.setForeground(ColorFG);
		title.setText(sAboutTitle);
		title.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, true));

		Table table = new Table(shell, SWT.MULTI | SWT.FULL_SELECTION | SWT.NO_FOCUS);
		table.setHeaderVisible(false);
		table.setLinesVisible(false);
		table.setBackground(toolsBar.ColorGainsboro);
		table.setFont(fontAbout);
		new TableColumn(table, SWT.LEFT);

		Runtime runtime = Runtime.getRuntime();
		TableItem ti;
		try {
			final Process proc = runtime.exec(command);
			String line;
			try (BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()))) {
				while ((line = reader.readLine()) != null) {
					ti = new TableItem(table, SWT.NONE);
					ti.setText(line);
				}
			}
		} catch (IOException ioe) { ioe.printStackTrace(); }

		table.getColumn(0).pack();
		shell.pack();
		Point n = shell.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		shell.setLocation((toolsBar.screenWidth / 2 - n.x / 2), (toolsBar.screenHeight / 2 - n.y / 2));
		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
}
//==========================================================================
