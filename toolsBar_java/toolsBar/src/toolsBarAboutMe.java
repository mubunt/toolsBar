//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
//------------------------------------------------------------------------------
class toolsBarAboutMe {
	//--------------------------------------------------------------------------
	private static final String sAboutTitle = "About this tool";
	private static final String sAboutTool = toolsBar.eol + toolsBar.toolName + toolsBar.eol + toolsBar.eol
			+ "Build " + toolsBarBuildInfo.getNumber() + " - " + toolsBarBuildInfo.getDate() + toolsBar.eol + toolsBar.eol
			+ "Copyright (c) 2016-2017, Michel RIZZO\n\n"
			+ "This program is free software; you can redistribute it and/or "
			+ "modify it under the terms of the GNU General Public License "
			+ "as published by the Free Software Foundation; either version 2 "
			+ "of the License, or (at your option) any later version.\n\n"
			+ "This program is distributed in the hope that it will be useful, "
			+ "but WITHOUT ANY WARRANTY; without even the implied warranty of "
			+ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
			+ "GNU General Public License for more details.";
	static final Font fontAbout = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	static final Font fontTool = new Font(Display.getDefault(), "Arial", 20, SWT.BOLD);
	static final Color ColorFG = new Color(Display.getDefault(), 10, 149, 184);
	//==========================================================================
	static void View() {
		Shell shell = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE);
		shell.setLayout(new FillLayout());
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.setBackground(toolsBar.ColorGainsboro);
		shell.setText(sAboutTitle);

		StyledText aboutwin = new StyledText(shell, SWT.WRAP | SWT.BORDER | SWT.CENTER);
		aboutwin.setFont(fontAbout);
		aboutwin.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		aboutwin.setText(sAboutTool);

		StyleRange styles = new StyleRange();
		styles.start = 0;
		styles.length = toolsBar.toolName.length() + 1;
		styles.font = fontTool;
		styles.fontStyle = SWT.BOLD;
		styles.foreground = ColorFG;
		aboutwin.setStyleRange(styles);

		shell.setSize(500, 300);
		shell.setLocation((toolsBar.screenWidth / 2 - 250), (toolsBar.screenHeight / 2 - 150));
		shell.open();
		while (!shell.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
}
//==========================================================================
