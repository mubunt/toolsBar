//------------------------------------------------------------------------------
// Copyright (c) 2016-2017, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
//------------------------------------------------------------------------------
class toolsBarPreferences {
	//--------------------------------------------------------------------------
	private static final String sPreferencesTitle	= "Preferences";
	private static Text txtToolsLocation;
	private static String strDirection;
	private static String strLocation;
	private static Shell shell2;
	static final Font fontLabel = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	static final Font fontWarning = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	static final Font fontButton = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	static final Font fontGroup = new Font(Display.getDefault(), "Arial", 9, SWT.BOLD | SWT.ITALIC);
	//==========================================================================
	static void View() {
		shell2 = new Shell(Display.getCurrent(), SWT.CLOSE | SWT.TITLE);
		shell2.setLayout(new GridLayout());
		shell2.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell2.setBackground(toolsBar.ColorGainsboro);
		shell2.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		shell2.setText(sPreferencesTitle);

		Composite container = new Composite(shell2, SWT.NONE);
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		GridLayout layout = new GridLayout(1, false);
		container.setLayout(layout);

		createToolsLocation(container);
		createLocation(container);
		createDirection(container);
		createWarning(container);
		createButton(container);

		shell2.pack();
		Point n = shell2.computeSize(SWT.DEFAULT, SWT.DEFAULT);
		shell2.setLocation((toolsBar.screenWidth / 2 - n.x / 2), (toolsBar.screenHeight / 2 - n.y / 2));
		shell2.open();
		while (!shell2.isDisposed())
			if (!Display.getCurrent().readAndDispatch())
				Display.getCurrent().sleep();
	}
	//==========================================================================
	private static void createToolsLocation(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(4, false));

		Label lbToolsLocation = new Label(area, SWT.NONE);
		lbToolsLocation.setText("Tools Location: ");
		lbToolsLocation.setFont(fontLabel);
		lbToolsLocation.setBackground(toolsBar.ColorGainsboro);
		lbToolsLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;

		txtToolsLocation = new Text(area, SWT.BORDER);
		txtToolsLocation.setLayoutData(gd);
		txtToolsLocation.setText(toolsBar.ToolsetRootDir);
		txtToolsLocation.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_WHITE));
		txtToolsLocation.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.setBackground(toolsBar.ColorGainsboro);
		open.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
				dlg.setFilterPath(toolsBar.home);
				String fn = dlg.open();
				if (fn != null)
					txtToolsLocation.setText(fn);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.setBackground(toolsBar.ColorGainsboro);
		help.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.OK | SWT.ICON_INFORMATION);
				messageBox.setMessage("Pathname of Tools repository (a.k.a. 'Toolset').");
				messageBox.open();
			}
		});
	}
	//==========================================================================
	private static void createLocation(Composite parent) {
		strLocation = String.valueOf(toolsBar.Location);

		Group location = new Group(parent, SWT.NONE);
		location.setFont(fontGroup);
		location.setText("  Location  ");
		location.setBackground(toolsBar.ColorGainsboro);
		location.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = true;
		location.setLayout(layout);
		location.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));
	
		Button tl = new Button(location, SWT.RADIO);
		tl.setText("Top / Left");
		tl.setFont(fontLabel);
		tl.setBackground(toolsBar.ColorGainsboro);
		tl.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button tc = new Button(location, SWT.RADIO);
		tc.setText("Top / Center");
		tc.setFont(fontLabel);
		tc.setBackground(toolsBar.ColorGainsboro);
		tc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button tr = new Button(location, SWT.RADIO);
		tr.setText("Top / Right");
		tr.setFont(fontLabel);
		tr.setBackground(toolsBar.ColorGainsboro);
		tr.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button ml = new Button(location, SWT.RADIO);
		ml.setText("Middle / Left");
		ml.setFont(fontLabel);
		ml.setBackground(toolsBar.ColorGainsboro);
		ml.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Label labinvisible = new Label(location, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		Button mr = new Button(location, SWT.RADIO);
		mr.setText("Middle / Right");
		mr.setFont(fontLabel);
		mr.setBackground(toolsBar.ColorGainsboro);
		mr.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button bl = new Button(location, SWT.RADIO);
		bl.setText("Bottom / Left");
		bl.setFont(fontLabel);
		bl.setBackground(toolsBar.ColorGainsboro);
		bl.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button bc = new Button(location, SWT.RADIO);
		bc.setText("Bottom / Center");
		bc.setFont(fontLabel);
		bc.setBackground(toolsBar.ColorGainsboro);
		bc.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		Button br = new Button(location, SWT.RADIO);
		br.setText("Bottom / Right");
		br.setFont(fontLabel);
		br.setBackground(toolsBar.ColorGainsboro);
		br.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));

		SelectionListener selectionListener = new SelectionAdapter () {
			public void widgetSelected(SelectionEvent e) {
				Button btn = (Button) e.getSource();
				if (btn == tl) strLocation = String.valueOf(toolsBar.cst_topleft);
				if (btn == tc) strLocation = String.valueOf(toolsBar.cst_topcenter);
				if (btn == tr) strLocation = String.valueOf(toolsBar.cst_topright);
				if (btn == ml) strLocation = String.valueOf(toolsBar.cst_middleleft);
				if (btn == mr) strLocation = String.valueOf(toolsBar.cst_middleright);
				if (btn == bl) strLocation = String.valueOf(toolsBar.cst_bottomleft);
				if (btn == bc) strLocation = String.valueOf(toolsBar.cst_bottomcenter);
				if (btn == br) strLocation = String.valueOf(toolsBar.cst_bottomright);
			}
		};

		tl.addSelectionListener(selectionListener);
		tc.addSelectionListener(selectionListener);
		tr.addSelectionListener(selectionListener);
		ml.addSelectionListener(selectionListener);
		mr.addSelectionListener(selectionListener);
		bl.addSelectionListener(selectionListener);
		bc.addSelectionListener(selectionListener);
		br.addSelectionListener(selectionListener);

		switch (toolsBar.Location) {
		case toolsBar.cst_topleft:
			tl.setSelection(true);
			break;
		case toolsBar.cst_topcenter:
			tc.setSelection(true);
			break;
		case toolsBar.cst_topright:
			tr.setSelection(true);
			break;
		case toolsBar.cst_middleleft:
			ml.setSelection(true);
			break;
		case toolsBar.cst_middleright:
			mr.setSelection(true);
			break;
		case toolsBar.cst_bottomleft:
			bl.setSelection(true);
			break;
		case toolsBar.cst_bottomcenter:
			bc.setSelection(true);
			break;
		case toolsBar.cst_bottomright:
			br.setSelection(true);
			break;
		default:
			strLocation = String.valueOf(toolsBar.cst_topleft);
			tl.setSelection(true);
			break;
		}

	}
	//==========================================================================
	private static void createDirection(Composite parent) {
		strDirection = String.valueOf(toolsBar.Direction);

		Group direction = new Group(parent, SWT.NONE);
		direction.setFont(fontGroup);
		direction.setText("  Direction  ");

		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		layout.makeColumnsEqualWidth = true;
		direction.setLayout(layout);
		direction.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false, 0, 0));

		Button ho = new Button(direction, SWT.RADIO);
		ho.setText("Horizontal");
		ho.setFont(fontLabel);
		ho.setBackground(toolsBar.ColorGainsboro);
		ho.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		ho.addSelectionListener(new SelectionAdapter () {
			public void widgetSelected(SelectionEvent __) {
				strDirection = "0";
			}
		});

		Button ve = new Button(direction, SWT.RADIO);
		ve.setText("Vertical");
		ve.setFont(fontLabel);
		ve.setBackground(toolsBar.ColorGainsboro);
		ve.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		ve.addSelectionListener(new SelectionAdapter () {
			public void widgetSelected(SelectionEvent __) {
				strDirection = "1";
			}
		});

		if (toolsBar.Direction == toolsBar.cst_vertical)
			ve.setSelection(true);
		else {
			ho.setSelection(true);
			strDirection = "0";
		}

		(new Label(direction, SWT.NO_FOCUS | SWT.HIDE_SELECTION))
				.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
	}
	//==========================================================================
	private static void createWarning(Composite parent) {
		Composite area4 = new Composite(parent, SWT.FILL);
		area4.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		area4.setLayout(new GridLayout(2, false));
		Label image = new Label(area4, SWT.LEFT);
		image.setImage(Display.getCurrent().getSystemImage(SWT.ICON_WARNING));
		image.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
		GridData dataWarn = new GridData();
		dataWarn.horizontalAlignment = SWT.BEGINNING;
		dataWarn.verticalAlignment = GridData.BEGINNING;
		dataWarn.grabExcessVerticalSpace = dataWarn.grabExcessHorizontalSpace = true;
		dataWarn.heightHint = 80;
		Label warning = new Label(area4, SWT.LEFT | SWT.WRAP);
		warning.setFont(fontWarning);
		warning.setBackground(toolsBar.ColorGainsboro);
		warning.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		warning.setLayoutData(dataWarn);
		warning.setText("WARNING:"
				+ toolsBar.eol + "\t- By clicking the 'SAVE' button, the preference file"
				+ toolsBar.eol + "\t\t" + toolsBar.propertiesFile + toolsBar.eol + "\t  will be modified."
				+ toolsBar.eol + "\t- You must restart '" + toolsBar.toolName + "' for the changes to take effect.");
	}
	//==========================================================================
	private static void createButton(Composite parent) {
		Composite buttonBar = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		layout.makeColumnsEqualWidth = true;
		buttonBar.setLayout(layout);

		final GridData data = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = false;
		buttonBar.setLayoutData(data);

		Label labinvisible1 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible2 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible3 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible3.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible4 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible4.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible5 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible5.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		Button save = new Button(buttonBar, SWT.PUSH);
		save.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		save.setText("Save");
		save.setFont(fontButton);
		save.setBackground(toolsBar.ColorGainsboro);
		save.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		save.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				toolsBar.properties.setProperty(toolsBar.propertyTools, txtToolsLocation.getText());
				toolsBar.properties.setProperty(toolsBar.propertyDirection, strDirection);
				toolsBar.properties.setProperty(toolsBar.propertyLocation, strLocation);
				toolsBar.savePropertyFile(toolsBar.properties, toolsBar.propertiesFile);
				shell2.dispose();
			}
		});
	}
}
//==========================================================================
